import cv2
import numpy as np

img = np.zeros((500,500,3),np.uint8)                    # Create a Black img
img[:] = 255,0,0                                        # Paint the img to Blue  (Blue,Green,Red)

# Draw a line (image, 1st_point, 2st_point, color, thickness)       (x,y)
cv2.line(img,(0,0),(img.shape[1],img.shape[0]),(0,0,255),thickness=3, lineType=0)

# Draw a rectangle (image, 1st_point, 2st_point, color, thickness)  (x,y)
cv2.rectangle(img,(100,100),(300,300),(0,0,255),2)
# Filled with color
cv2.rectangle(img,(400,100),(450,150),(0,0,255),cv2.FILLED)

# Draw a circle (image, center, radius, color, thickness)
cv2.circle(img,(200,300),50,(0,0,255),2)

# Insert text
cv2.putText(img,"Hello OpenCV",(50,450),cv2.FONT_ITALIC,1,(0,0,255),1)

cv2.imshow("Image", img)
cv2.waitKey(0)