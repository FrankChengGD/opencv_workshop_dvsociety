import cv2
import os

cv2_base_dir = os.path.dirname(os.path.abspath(cv2.__file__))
model_path = os.path.join(cv2_base_dir, 'data/haarcascade_frontalface_default.xml')
model = cv2.CascadeClassifier(model_path)

cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)
while True:
    success, frame = cap.read()
    if not success:
        break
    frame_Gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = model.detectMultiScale(frame_Gray, 1.1, 4)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        cv2.imshow("Result", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
