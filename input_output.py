import cv2
# print("Successfully import opencv")

# # Load image
# img_path = 'OpenCV_Logo.jpg'
# img = cv2.imread(img_path)
# print(img.shape)            #(190, 340, 3) -> (Height, Width, Color Channel)
# cv2.imshow('opencv logo', img)
# cv2.waitKey(0)              # change to 1000
# cv2.imwrite('output.png',img)

# Load video
video_path = 'opencv_test.mp4'
cap = cv2.VideoCapture(video_path)
total_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))    # 144 frames   
fps = int(cap.get(cv2.CAP_PROP_FPS))                    # 30 fps
size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))    # (854, 480)
print(total_frame, fps, size)

# out = cv2.VideoWriter('output.mp4', cv2.VideoWriter_fourcc('m','p','4','v'), fps, size)

while True:
    success, frame = cap.read()
    if not success:
        break
    # out.write(frame)
    cv2.imshow("Video",frame)
    if cv2.waitKey(1) & 0xFF==ord('q'):
        break
cap.release()
# out.release()

# # Load from Webcam
# cap = cv2.VideoCapture(0)
# cap.set(3,640)                          # Width
# cap.set(4,480)                          # Height
# while True:
#     success,img = cap.read()
#     cv2.imshow("Webcam",img)
#     if cv2.waitKey(1) & 0xFF==ord('q'):
#         break