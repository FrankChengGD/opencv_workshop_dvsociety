import cv2

img_path = 'OpenCV_Logo.jpg'
img = cv2.imread(img_path)
# img_Gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# img_RGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# img_Blur = cv2.GaussianBlur(img, (5,5),0)            # (5,5) is ksize, need to be set with odd numbers
# img_Canny = cv2.Canny(img,100,100)
# cv2.imshow('img', img)
# cv2.imshow('img_Gray', img_Gray)
# cv2.imshow('img_RGB', img_RGB)
# cv2.imshow('img_Blur', img_Blur)
# cv2.imshow('img_Canny', img_Canny)
# cv2.waitKey(0)

img_Resize = cv2.resize(img, (150, 300))                # (150, 300) -> (Width, Height)
img_Crop = img[0:200,70:250]                            # [y1:y2, x1:x2]
cv2.imshow('img', img)
cv2.imshow('img_Resize', img_Resize)
cv2.imshow('img_Crop', img_Crop)
cv2.waitKey(0)